import { createRouter, createWebHistory } from "vue-router"
import Simple from "../views/Simple.vue"
import Drawer from "../views/Drawer.vue"
import List from "../views/List.vue"
import Modal from "../views/Modal.vue"
import Stagger from "../views/Stagger.vue"
import State from "../views/State.vue"

const routes = [
  {
    path: "/",
    name: "Modal",
    component: Modal
  },
  {
    path: "/list",
    name: "List",
    component: List
  },
  {
    path: "/drawer",
    name: "Drawer",
    component: Drawer
  },
  {
    path: "/simple",
    name: "Simple",
    component: Simple
  },
  {
    path: "/stagger",
    name: "Stagger",
    component: Stagger
  },
  {
    path: "/state",
    name: "State",
    component: State
  },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
