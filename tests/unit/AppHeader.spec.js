import AppHeader from '../../src/components/AppHeader.vue'
import { mount } from '@vue/test-utils'

describe('AppHeader', () => {
    test('If user is not logged in, do not show logout button', () => {
        const wrapper = mount(AppHeader)
        expect(wrapper.find('button').attributes().style).toMatch(/display: none;/)
    })

    test('If user is logged in, show logout button', async () => {
        let wrapper = mount(AppHeader);
        wrapper.vm.loggedIn = true
        await wrapper.vm.$nextTick()
        expect(wrapper.find('button').attributes().style).not.toMatch(/display: none;/)
    })
})