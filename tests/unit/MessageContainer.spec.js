import MessageContainer from '../../src/components/MessageContainer'
import { mount } from '@vue/test-utils'

jest.mock('../../src/components/MessageDisplay', () => ({
    template: '<p data-test-id="message">Hello from the db!</p>'
}))

describe('MessageContainer', () => {
    it('Wraps MessageDisplay component', () => {
        const wrapper = mount(MessageContainer)

        const message = wrapper.find('[data-test-id="message"]').element.textContent
        expect(message).toEqual('Hello from the db!')
    })
})